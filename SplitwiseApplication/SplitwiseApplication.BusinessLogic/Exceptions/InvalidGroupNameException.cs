﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplitwiseApplication.BusinessLogic.Exceptions
{
    public class InvalidGroupNameException : BaseException
    {
        public InvalidGroupNameException() : base(400, "Invalid group name")
        {

        }
    }
}
