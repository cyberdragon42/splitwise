﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SplitwiseApplication.DataAccess.Migrations
{
    public partial class FixedModels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Confirmed",
                table: "Payments",
                newName: "IsConfirmed");

            migrationBuilder.CreateIndex(
                name: "IX_Payments_FromUserId",
                table: "Payments",
                column: "FromUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Payments_Users_FromUserId",
                table: "Payments",
                column: "FromUserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Payments_Users_FromUserId",
                table: "Payments");

            migrationBuilder.DropIndex(
                name: "IX_Payments_FromUserId",
                table: "Payments");

            migrationBuilder.RenameColumn(
                name: "IsConfirmed",
                table: "Payments",
                newName: "Confirmed");
        }
    }
}
