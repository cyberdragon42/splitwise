﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplitwiseApplication.BusinessLogic.Services.ServiceModels
{
    public class Payment
    {
        public int FromUserId { get; set; }
        public int ToUserId { get; set; }
        public int GroupId { get; set; }
        public string Description { get; set; }
        public DateTime Time { get; set; }
        public decimal Amount { get; set; }
    }
}
