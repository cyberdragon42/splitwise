﻿using Microsoft.EntityFrameworkCore;
using SplitwiseApplication.DataAccess.Context;
using SplitwiseApplication.DataAccess.Repositories.Interfaces;
using SplitwiseApplication.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ardalis.Specification;

namespace SplitwiseApplication.DataAccess.Repositories.Implementations
{
    public class UserRepository : BaseRepository<User, int, SplitwiseContext>, IUserRepository
    {
        public UserRepository(SplitwiseContext context) : base(context)
        {

        }

        public async Task<User> GetByNameAsync(string name)
        {
            return await _dbSet.FirstOrDefaultAsync(x => x.Name == name);
        }

    }
}
