﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplitwiseApplication.BusinessLogic.Services.ServiceModels
{
    public class Expense
    {
        public DateTime Time { get; set; }
        public string Description { get; set; }
        public int UserId { get; set; }
        public int GroupId { get; set; }
        public decimal TotalAmount { get; set; }
    }
}
