﻿using Ardalis.Specification;
using Ardalis.Specification.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SplitwiseApplication.DataAccess.Repositories.Interfaces;
using SplitwiseApplication.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplitwiseApplication.DataAccess.Repositories.Implementations
{
    public class BaseRepository<T, TKey, TContext> : IRepository<T, TKey>
         where T : class, Domain.Interfaces.IEntity<TKey>
         where TContext : DbContext, IUnitOfWork
    {
        protected readonly DbSet<T> _dbSet;
        protected readonly TContext _context;
        protected readonly ISpecificationEvaluator specificationEvaluator;


        public BaseRepository(TContext context)
        {
            _dbSet = context.Set<T>();
            specificationEvaluator = new SpecificationEvaluator();
        }

        public IUnitOfWork UnitOfWork => _context;

        public async Task<IList<T>> GetManyAsync(Specification<T> specification)
        {
            return await specificationEvaluator.GetQuery(_dbSet, specification).ToListAsync();
        }


        public async Task<T> GetSingleElementAsync(Specification<T> specification)
        {
            return await specificationEvaluator.GetQuery(_dbSet, specification).FirstOrDefaultAsync();
        }

        public async Task DeleteAsync(T item)
        {
            _dbSet.Remove(item);
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _dbSet.ToListAsync();
        }

        public async Task<T> GetByIdAsync(TKey id)
        {
            return await _dbSet.FirstOrDefaultAsync(x =>
            x.Id.Equals(id));
        }

        public Task<T> InsertAsync(T item)
        {
            var entity = _dbSet.Add(item);
            return Task.FromResult(entity.Entity);
        }

        public Task UpdateAsync(T item)
        {
            _context.Entry(item).State = EntityState.Modified;
            return Task.CompletedTask;
        }
    }
}

