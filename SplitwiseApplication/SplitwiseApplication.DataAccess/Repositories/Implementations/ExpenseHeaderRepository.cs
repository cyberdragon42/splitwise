﻿using Ardalis.Specification;
using Ardalis.Specification.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SplitwiseApplication.DataAccess.Context;
using SplitwiseApplication.DataAccess.Repositories.Interfaces;
using SplitwiseApplication.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplitwiseApplication.DataAccess.Repositories.Implementations
{
    public class ExpenseHeaderRepository : BaseRepository<ExpenseHeader, int, SplitwiseContext>, IExpenseHeaderRepository
    {
        public ExpenseHeaderRepository(SplitwiseContext context) : base(context)
        {

        }
    }
}
