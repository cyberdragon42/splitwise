﻿using SplitwiseApplication.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplitwiseApplication.Domain.Models
{
    public class User : IEntity<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<ExpenseLine> ExpenseLines { get; set; }
        public ICollection<ExpenseHeader> ExpenseHeaders { get; set; }
        public ICollection<Payment> FromUserPayments { get; set; }
        public ICollection<Payment> ToUserPayments { get; set; }
        public ICollection<Group> Groups { get; set; }

    }
}
