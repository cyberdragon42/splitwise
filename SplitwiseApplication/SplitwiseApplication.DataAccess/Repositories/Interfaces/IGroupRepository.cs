﻿using SplitwiseApplication.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplitwiseApplication.DataAccess.Repositories.Interfaces
{
    interface IGroupRepository : IRepository<Group, int>
    {
        Task<Group> GetByNameAsync(string name);
    }
}
