﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Ardalis.Specification;
using Ardalis.Specification.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SplitwiseApplication.DataAccess.Context;
using SplitwiseApplication.DataAccess.Repositories.Interfaces;
using SplitwiseApplication.Domain.Models;

namespace SplitwiseApplication.DataAccess.Repositories.Implementations
{
    public class PaymentsRepository : BaseRepository<Payment, int, SplitwiseContext>, IPaymentRepository
    {
        public PaymentsRepository(SplitwiseContext context) : base(context)
        {

        }
    }
}
