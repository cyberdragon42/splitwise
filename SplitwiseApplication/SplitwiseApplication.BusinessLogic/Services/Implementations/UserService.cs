﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SplitwiseApplication.BusinessLogic.Services.Interfaces;
using SplitwiseApplication.BusinessLogic.Specifications.Implementations;
using SplitwiseApplication.DataAccess.Context;
using SplitwiseApplication.DataAccess.Repositories.Implementations;
using SplitwiseApplication.DataAccess.Repositories.Interfaces;
using SplitwiseApplication.Domain.Models;

namespace SplitwiseApplication.BusinessLogic.Services.Implementations
{
    public class UserService : IUserService
    {
        private readonly IRepository<Group, int> _groupRepository;
        private readonly IRepository<User, int> _userRepository;

        public UserService(IRepository<Group, int> groupRepository, IRepository<User, int> userRepository)
        {
            _groupRepository = groupRepository;
            _userRepository = userRepository;
        }

        public async Task AddUserToGroup(int userId, int groupId)
        {
            //get user by such id
            var user = await _userRepository.GetByIdAsync(userId);

            //get group by such id
            var group = await _groupRepository.GetByIdAsync(groupId);

            if (user == null || group == null)
            {
                throw new Exception("User or group doesn't exist");
            }

            //add user to group
            user.Groups.Add(group);
            group.Users.Add(user);
            await _userRepository.UnitOfWork.SaveChangesAsync();
        }
    }
}
