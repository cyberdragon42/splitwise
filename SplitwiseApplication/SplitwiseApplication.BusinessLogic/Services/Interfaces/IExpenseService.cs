﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SplitwiseApplication.BusinessLogic.Services.ServiceModels;
using SplitwiseApplication.Domain.Models;

namespace SplitwiseApplication.BusinessLogic.Services.Interfaces
{
    public interface IExpenseService
    {
        public Task<ExpenseHeader> CreateExpense(Expense expense);
        public Task<Debt> CountDebt(int groupId, int fromUserId, int forUserId);
    }
}
