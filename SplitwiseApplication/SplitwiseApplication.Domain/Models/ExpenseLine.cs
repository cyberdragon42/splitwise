﻿using SplitwiseApplication.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplitwiseApplication.Domain.Models
{
    public class ExpenseLine: IEntity<int>
    {
        public int Id { get; set; }
        public decimal Amount { get; set; }

        public User User { get; set; }
        public ExpenseHeader ExpenseHeader { get; set; }

        public int ForUserId { get; set; }
        public int ExpenseHeaderId { get; set; }
    }
}
