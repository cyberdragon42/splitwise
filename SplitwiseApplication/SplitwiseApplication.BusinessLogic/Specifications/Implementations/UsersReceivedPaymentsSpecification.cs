﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ardalis.Specification.EntityFrameworkCore;
using Ardalis.Specification;
using SplitwiseApplication.Domain.Models;
using Microsoft.EntityFrameworkCore;
using SplitwiseApplication.BusinessLogic.Helpers;

namespace SplitwiseApplication.BusinessLogic.Specifications.Implementations
{
    /// <summary>
    /// Returns confirmed payments associated with a user in a group
    /// </summary>
    public class UsersReceivedPaymentsSpecification: Specification<Payment>
    {
        public UsersReceivedPaymentsSpecification(int userId, int groupId)
        {
            Query.Where(x => SpecificationHelper.IsPaymentAssociatedWithUser(x, userId, groupId) && x.IsConfirmed);
        }
    }
}

