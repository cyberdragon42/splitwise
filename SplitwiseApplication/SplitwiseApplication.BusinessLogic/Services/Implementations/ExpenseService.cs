﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SplitwiseApplication.BusinessLogic.Services.Interfaces;
using SplitwiseApplication.BusinessLogic.Services.ServiceModels;
using SplitwiseApplication.BusinessLogic.Specifications.Implementations;
using SplitwiseApplication.DataAccess.Context;
using SplitwiseApplication.DataAccess.Repositories.Implementations;
using SplitwiseApplication.DataAccess.Repositories.Interfaces;
using SplitwiseApplication.Domain.Models;
using SplitwiseApplication.BusinessLogic.Exceptions;

namespace SplitwiseApplication.BusinessLogic.Services.Implementations
{
    public class ExpenseService : IExpenseService
    {
        private readonly IRepository<ExpenseHeader, int> _expenseRepository;
        private readonly IRepository<User, int> _usersRepository;

        public ExpenseService(IRepository<ExpenseHeader, int> expenseRepository,IRepository<User, int> userRepository)
        {
            _expenseRepository = expenseRepository;
            _usersRepository = userRepository;
        }

        public async Task<Debt> CountDebt(int groupId, int fromUserId, int forUserId)
        {
            //find group expenses
            var specification = new GroupExpensesSpecification(groupId);
            var groupExpenses = await _expenseRepository.GetManyAsync(specification);

            //count sum of fromuserexpenses to foruser
            decimal debtAmount = 0;
            foreach(var ex in groupExpenses.Where(x=>x.UserId == fromUserId))
            {
                foreach(var el in ex.ExpenseLines)
                {
                    if (el.ForUserId == forUserId)
                        debtAmount += el.Amount;
                }
            }

            //count reverse
            foreach (var ex in groupExpenses.Where(x => x.UserId == forUserId))
            {
                foreach (var el in ex.ExpenseLines)
                {
                    if (el.ForUserId == fromUserId)
                        debtAmount -= el.Amount;
                }
            }

            var debt = new Debt()
            {
                Amount = debtAmount,
                FromUserId = fromUserId,
                ForUserId = forUserId
            };

            return debt;
        }

        public async Task<ExpenseHeader> CreateExpense(Expense expense)
        {
            //create expense header
            var expenseHeader = new ExpenseHeader()
            {
                Description = expense.Description,
                GroupId = expense.GroupId,
                UserId = expense.UserId,
                Time = expense.Time
            };

            //find users of group
            var specification = new UsersGroupsSpecification(expense.GroupId);
            var users = await _usersRepository.GetManyAsync(specification);

            //split total amount
            var amountPerUser = expense.TotalAmount / users.Count;

            //add expense lines
            foreach(var u in users)
            {
                if (u.Id != expense.UserId)
                {
                    var expenseLine = new ExpenseLine() { ForUserId = u.Id, Amount = amountPerUser };
                    expenseHeader.ExpenseLines.Add(expenseLine);
                }
            }

            //add and save
            var createdHeader = await _expenseRepository.InsertAsync(expenseHeader);
            await _expenseRepository.UnitOfWork.SaveChangesAsync();
            return createdHeader;
        }
    }
}
