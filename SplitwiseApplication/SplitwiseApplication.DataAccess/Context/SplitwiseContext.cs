﻿using Microsoft.EntityFrameworkCore;
using SplitwiseApplication.DataAccess.Repositories.Interfaces;
using SplitwiseApplication.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplitwiseApplication.DataAccess.Context
{
    public class SplitwiseContext: DbContext, IUnitOfWork
    {
        public DbSet<User> Users { get; set; }
        public DbSet<ExpenseLine> ExpenseLines { get; set; }
        public DbSet<ExpenseHeader> ExpenseHeaders { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Payment> Payments { get; set; }

        public SplitwiseContext(DbContextOptions<SplitwiseContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Group>(b =>
            {
                b.HasKey(x => x.Id);
                b.Property(x => x.Name).HasMaxLength(30);
            });

            modelBuilder.Entity<User>(b =>
            {
                b.HasKey(x => x.Id);
                b.Property(x => x.Name).HasMaxLength(30);
            });

            modelBuilder.Entity<Payment>(b =>
            {
                b.HasKey(x => x.Id);

                b.Property(x => x.Description).HasMaxLength(50);

                b.HasOne(x => x.Group)
                    .WithMany(g => g.Payments)
                    .HasForeignKey(x => x.GroupId);

                b.HasOne(x => x.FromUser)
                    .WithMany(p => p.FromUserPayments)
                    .HasForeignKey(x => x.FromUserId);

                b.HasOne(x => x.ToUser)
                    .WithMany(p => p.ToUserPayments)
                    .HasForeignKey(x => x.ToUserId);

            });

            modelBuilder.Entity<ExpenseHeader>(b =>
            {
                b.HasKey(x => x.Id);
                b.HasOne(x => x.User)
                    .WithMany(u => u.ExpenseHeaders)
                    .HasForeignKey(x => x.UserId);

                b.HasOne(x => x.Group)
                    .WithMany(g => g.ExpenseHeaders)
                    .HasForeignKey(x => x.GroupId);

                b.Property(x => x.Description).HasMaxLength(50);

            });

            modelBuilder.Entity<ExpenseLine>(b =>
            {
                b.HasKey(x => x.Id);
                b.HasOne(x => x.User)
                    .WithMany(u => u.ExpenseLines)
                    .HasForeignKey(x => x.ForUserId);

                b.HasOne(x => x.ExpenseHeader)
                    .WithMany(e => e.ExpenseLines)
                    .HasForeignKey(x => x.ExpenseHeaderId);
            });


            modelBuilder.Entity<Group>()
                .HasMany(g=>g.Users)
                    .WithMany(u=>u.Groups)
                    .UsingEntity(j => j.ToTable("UserGroup"));
        }
    }
}
