﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SplitwiseApplication.Middleware
{
    using Microsoft.AspNetCore.Http;
    using SplitwiseApplication.BusinessLogic.Exceptions;
    using System.Threading.Tasks;

    public class ExceptionHandlingMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionHandlingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next.Invoke(context);
            }

            catch (BaseException e)
            {
                context.Response.StatusCode = e.StatusCode;
                await context.Response.WriteAsync(e.Message);
            } 

            catch (Exception)
            {
                context.Response.StatusCode = 500;
                await context.Response.WriteAsync("An unexpected error occured");
            }
        }
    }
}
