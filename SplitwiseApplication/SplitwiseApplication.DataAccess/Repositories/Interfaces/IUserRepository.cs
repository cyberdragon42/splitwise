﻿using SplitwiseApplication.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplitwiseApplication.DataAccess.Repositories.Interfaces
{
    interface IUserRepository : IRepository<User, int>
    {
        Task<User> GetByNameAsync(string name);
    }
}
