﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplitwiseApplication.BusinessLogic.Services.Interfaces
{
    public interface IUserService
    {
        public Task AddUserToGroup(int userId, int groupId);
    }
}
