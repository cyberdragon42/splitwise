﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SplitwiseApplication.BusinessLogic.Exceptions
{
    public class BaseException: Exception
    {
        public int StatusCode { get; set; }

        public BaseException(int statusCode, string message) : base(message)
        {
            StatusCode = statusCode;
        }
    }
}
