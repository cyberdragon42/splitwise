﻿using Ardalis.Specification;
using Ardalis.Specification.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SplitwiseApplication.DataAccess.Context;
using SplitwiseApplication.DataAccess.Repositories.Interfaces;
using SplitwiseApplication.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplitwiseApplication.DataAccess.Repositories.Implementations
{
    public class GroupRepository : BaseRepository<Group, int, SplitwiseContext>, IGroupRepository
    {
        public GroupRepository(SplitwiseContext context) : base(context)
        {

        }

        public async Task<Group> GetByNameAsync(string name)
        {
            return await _dbSet.FirstOrDefaultAsync(x => x.Name == name);
        }
    }
}
