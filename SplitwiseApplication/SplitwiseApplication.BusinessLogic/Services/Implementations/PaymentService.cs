﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SplitwiseApplication.BusinessLogic.Services.Interfaces;
using SplitwiseApplication.BusinessLogic.Services.ServiceModels;
using SplitwiseApplication.BusinessLogic.Specifications.Implementations;
using SplitwiseApplication.DataAccess.Context;
using SplitwiseApplication.DataAccess.Repositories.Implementations;
using SplitwiseApplication.DataAccess.Repositories.Interfaces;
using SplitwiseApplication.Domain.Models;
using SplitwiseApplication.BusinessLogic.Exceptions;

namespace SplitwiseApplication.BusinessLogic.Services.Implementations
{
    public class PaymentService : IPaymentService
    {
        private readonly IRepository<Domain.Models.Payment, int> _paymentsRepository;
        private readonly IRepository<Group, int> _groupRepository;
        private readonly IRepository<User, int> _userRepository;


        public PaymentService(IRepository<Group, int> groupRepository,
            IRepository<User, int> userRepository, IRepository<Domain.Models.Payment, int> paymentRepository)
        {
            _paymentsRepository = paymentRepository;
            _groupRepository = groupRepository;
            _userRepository = userRepository;
        }

        public async Task ConfirmPayment(int paymentId, int toUserId)
        {
            //search payment by id
            var payment = await _paymentsRepository.GetByIdAsync(paymentId);
            if(payment == null)
            {
                throw new EntityDoesNotExistException("Payment");
            }

            if(payment.ToUserId == toUserId)
            {
                //make payment isconfirmed true
                payment.IsConfirmed = true;
                await _paymentsRepository.UpdateAsync(payment);

                //save changes
                await _paymentsRepository.UnitOfWork.SaveChangesAsync();
            }
        }

        public async Task<Domain.Models.Payment> CreatePayment(ServiceModels.Payment paymentInfo)
        {
            //find users
            var fromUser = await _userRepository.GetByIdAsync(paymentInfo.FromUserId);
            var toUser = await _userRepository.GetByIdAsync(paymentInfo.ToUserId);
            //if they dont exist throw exception
            if(fromUser == null || toUser == null)
            {
                throw new EntityDoesNotExistException("User");
            }

            //find group
            var group = await _groupRepository.GetByIdAsync(paymentInfo.GroupId);
            //if group doesnt exist throw exception
            if(group == null)
            {
                throw new EntityDoesNotExistException("Group");
            }
            //if users are not group members throw exceptions
            if(!fromUser.Groups.Any(g=>g.Id == paymentInfo.GroupId) 
                || !toUser.Groups.Any(g => g.Id == paymentInfo.GroupId))
            {
                throw new ArgumentException("Incorrect payment info");
            }

            if (paymentInfo == null)
            {
                throw new EntityDoesNotExistException("Payment");
            }


            var payment = new Domain.Models.Payment()
            {
                Amount = paymentInfo.Amount,
                Description = paymentInfo.Description,
                FromUserId = paymentInfo.FromUserId,
                ToUserId = paymentInfo.ToUserId,
                Time = paymentInfo.Time,
                IsConfirmed = false,
                GroupId = paymentInfo.GroupId
            };

            var createdPayment = await _paymentsRepository.InsertAsync(payment);
            await _paymentsRepository.UnitOfWork.SaveChangesAsync();

            return createdPayment;
        }
    }
}
