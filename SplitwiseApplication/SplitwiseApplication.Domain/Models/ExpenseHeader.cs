﻿using SplitwiseApplication.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplitwiseApplication.Domain.Models
{
    public class ExpenseHeader : IEntity<int>
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime Time { get; set; }

        public ICollection<ExpenseLine> ExpenseLines { get; set; }

        public int UserId { get; set; }
        public int GroupId { get; set; }
        public User User { get; set; }
        public Group Group { get; set; }
    }
}
