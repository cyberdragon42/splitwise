﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplitwiseApplication.BusinessLogic.Services.ServiceModels
{
    public class Debt
    {
        public int ForUserId { get; set; }
        public int FromUserId { get; set; }
        public decimal Amount { get; set; }
    }
}
