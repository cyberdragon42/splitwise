﻿using SplitwiseApplication.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplitwiseApplication.Domain.Models
{
    public class Group : IEntity<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<ExpenseHeader> ExpenseHeaders { get; set; }
        public ICollection<Payment> Payments { get; set; }
        public ICollection<User> Users { get; set; }
    }
}
