﻿
using Ardalis.Specification;
using SplitwiseApplication.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplitwiseApplication.DataAccess.Repositories.Interfaces
{
    public interface IRepository<T, TKey> where T : Domain.Interfaces.IEntity<TKey>
    {
        public IUnitOfWork UnitOfWork { get; }
        Task<T> GetByIdAsync(TKey id);
        Task<IEnumerable<T>> GetAllAsync();
        Task<T> InsertAsync(T item);
        Task UpdateAsync(T item);
        Task DeleteAsync(T item);
        Task<IList<T>> GetManyAsync(Specification<T> specification);
        Task<T> GetSingleElementAsync(Specification<T> specification);
    }
}
