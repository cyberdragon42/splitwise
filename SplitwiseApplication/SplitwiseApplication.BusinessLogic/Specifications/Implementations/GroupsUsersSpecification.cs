﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ardalis.Specification;
using Ardalis.Specification.EntityFrameworkCore;
using SplitwiseApplication.Domain.Models;

namespace SplitwiseApplication.BusinessLogic.Specifications.Implementations
{
    /// <summary>
    /// Returns users of the group
    /// </summary>
    public class GroupsUsersSpecification: Specification<Group>
    {
        public GroupsUsersSpecification(int groupId)
        {
            Query.Where(x => x.Id == groupId).Include(y => y.Users);
        }
    }
}
