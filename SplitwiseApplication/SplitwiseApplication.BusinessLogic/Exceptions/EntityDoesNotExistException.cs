﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplitwiseApplication.BusinessLogic.Exceptions
{
    public class EntityDoesNotExistException : BaseException
    {
        public EntityDoesNotExistException(string entityName):base(400, entityName+" does not exist")
        {

        }
    }
}
