﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SplitwiseApplication.BusinessLogic.Services.Interfaces;
using SplitwiseApplication.BusinessLogic.Specifications.Implementations;
using SplitwiseApplication.DataAccess.Context;
using SplitwiseApplication.DataAccess.Repositories;
using SplitwiseApplication.DataAccess.Repositories.Implementations;
using SplitwiseApplication.DataAccess.Repositories.Interfaces;
using SplitwiseApplication.Domain.Models;
using SplitwiseApplication.BusinessLogic.Exceptions;

namespace SplitwiseApplication.BusinessLogic.Services.Implementations
{
    public class GroupService : IGroupService
    {
        private readonly IRepository<Group, int> _groupRepository;

        public GroupService(IRepository<Group, int> groupRepository)
        {
            _groupRepository = groupRepository;
        }

        public async Task<Group> CreateGroup(string groupName)
        {
            if(string.IsNullOrWhiteSpace(groupName))
            {
                throw new InvalidGroupNameException();
            }

            //create group
            Group group = new Group() { Name = groupName };

            //insert group
            var createdGroup = await _groupRepository.InsertAsync(group);
            await _groupRepository.UnitOfWork.SaveChangesAsync();
            return createdGroup;
        }
    }
}
