﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SplitwiseApplication.Domain.Models;

namespace SplitwiseApplication.BusinessLogic.Helpers
{
    public static class SpecificationHelper
    {
        public static bool IsPaymentAssociatedWithUser(Payment payment, int userId, int groupId)
        {
            return payment.GroupId == groupId && (payment.FromUserId == userId || payment.ToUserId == userId);
        }
    }
}
