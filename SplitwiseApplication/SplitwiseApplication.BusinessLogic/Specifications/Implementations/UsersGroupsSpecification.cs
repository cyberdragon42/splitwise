﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ardalis.Specification;
using Ardalis.Specification.EntityFrameworkCore;
using SplitwiseApplication.Domain.Models;

namespace SplitwiseApplication.BusinessLogic.Specifications.Implementations
{
    /// <summary>
    /// Returns groups of the user
    /// </summary>
    public class UsersGroupsSpecification: Specification<User>
    {
        public UsersGroupsSpecification(int userId)
        {
            Query.Where(g => g.Id == userId).Include(x => x.Groups);
        }
    }
}
