﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ardalis.Specification;
using Ardalis.Specification.EntityFrameworkCore;
using SplitwiseApplication.Domain.Models;

namespace SplitwiseApplication.BusinessLogic.Specifications.Implementations
{
    /// <summary>
    /// Returns expenses of the group
    /// </summary>
    public class GroupExpensesSpecification: Specification<ExpenseHeader>
    {
        public GroupExpensesSpecification(int groupId)
        {
            Query.Where(x => x.GroupId == groupId);
        }
    }
}
