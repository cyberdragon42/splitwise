﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SplitwiseApplication.BusinessLogic.Services.ServiceModels;
using SplitwiseApplication.DataAccess.Context;
using SplitwiseApplication.DataAccess.Repositories.Implementations;
using SplitwiseApplication.Domain.Models;

namespace SplitwiseApplication.BusinessLogic.Services.Interfaces
{
    public interface IPaymentService
    {
        public Task<Domain.Models.Payment> CreatePayment(ServiceModels.Payment payment);
        public Task ConfirmPayment(int paymentId, int toUserId);

    }
}
